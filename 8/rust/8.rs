use std::env;
use std::fs;

fn main() {
    // Parse Input
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
    let mut result_part1: usize = 0;
    for line in contents.lines() {
        let (_input_raw, output_raw) = line.split_once('|').unwrap();
        let res: usize = output_raw
                              .split_ascii_whitespace()
                              .filter(|out| match out.len() {
                                2 | 4 | 3 | 7 => true,
                                _ => false
                              })
                              .count();
        //println!("{:?}", count);
        result_part1 += res;
    }


    println!("Part1: Val is: {}", result_part1);
}
