use std::env;
use std::fs;
use std::collections::HashMap;
use std::cmp::{ min, max };

struct Victor {
    x1: i32,
    y1: i32,
    x2: i32,
    y2: i32,
}

fn solve_part_one(vic: Vec<Victor>) -> usize {
    let mut board = HashMap::new();
    for v in vic {
        if v.x1 == v.x2 {
            for i in min(v.y1, v.y2)..=max(v.y1, v.y2) {
                *board.entry((v.x1, i)).or_insert(0) += 1;
            }
        } else if v.y1 == v.y2 {
            for i in min(v.x1, v.x2)..=max(v.x1, v.x2) {
                *board.entry((i, v.y1)).or_insert(0) += 1;
            }
        }
    }
    let res = board.values().filter(|&b| *b >= 2).count();

    return res;
}

fn solve_part_two(vic: Vec<Victor>) -> usize {
    let mut board = HashMap::new();
    for v in vic {
        if v.x1 == v.x2 {
            for i in min(v.y1, v.y2)..=max(v.y1, v.y2) {
                *board.entry((v.x1, i)).or_insert(0) += 1;
            }
        } else if v.y1 == v.y2 {
            for i in min(v.x1, v.x2)..=max(v.x1, v.x2) {
                *board.entry((i, v.y1)).or_insert(0) += 1;
            }
        } else {
            let direction = if (v.x1 < v.x2) == (v.y1 < v.y2) { 1 } else { -1 };
            for i in min(0, v.x2 - v.x1)..=max(0, v.x2 - v.x1) {
                let new = (v.x1 + i, v.y1 + i * direction);
                *board.entry(new).or_insert(0) += 1;
            }
        }
    }
    let res = board.values().filter(|&b| *b >= 2).count();

    return res;
}

fn main() {
    // PArse Input
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let part = &args[2];

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
    //Bing srwan numbers
    let mut v2: Vec<Victor> = Vec::new();

    for line in contents.trim().split('\n') {
        let (start, end) = line.trim().split_once(" -> ").unwrap();
        let (str_x1, str_y1) = start.trim().split_once(',').unwrap();
        let (str_x2, str_y2) = end.trim().split_once(',').unwrap();
        let v = Victor { x1: str_x1.parse::<i32>().unwrap(),
                         y1: str_y1.parse::<i32>().unwrap(),
                         x2: str_x2.parse::<i32>().unwrap(),
                         y2: str_y2.parse::<i32>().unwrap(),};
        //println!("{} {} {} {}", v.x1, v.y1 , v.x2, v.y2);
        v2.push(v);
    }

    if part == "1" {
        // Part1
        let res = solve_part_one(v2);
        println!("Part1: Val is: {}", res);
    } else {
        // Part2
        let res2 = solve_part_two(v2);
        println!("Part2: Val is: {}", res2);
    }
}
