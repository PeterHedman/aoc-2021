use std::env;
use std::fs;

fn solve_part_1(values: Vec<usize>, days: usize) -> usize {
    let _vals: Vec<usize> = values.clone();
    let mut total_fish: [usize; 9] = [0; 9];
    let mut fish: usize = 0;
    for val in values {
        total_fish[val] += 1;
    }
    for _i in 0..days {
        //every 6 or 8 days new fish are created
        // initial values are only the offset when this happens
        total_fish.rotate_left(1);
        total_fish[6] += total_fish[8];
    }
    for f in 0..total_fish.len() {
        fish += total_fish[f];
    }
    return fish;
}

fn main() {
    // PArse Input
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let part = &args[2];

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
    //Bing srwan numbers
    let values = contents.split(',').map(|s| s.trim().parse::<usize>().unwrap()).collect::<Vec<usize>>();

    //println!("{:?}", values);


    if part == "1" {
        // Part1
        let res = solve_part_1(values, 80);
        println!("Part1: Val is: {}", res);
    } else {
        // Part2
        let res2 = solve_part_1(values, 256);
        println!("Part2: Val is: {}", res2);
    }
}
