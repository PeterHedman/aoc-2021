# AOC-2021

Advent of code 2021 edition

## Goals
* Learning rust

## Getting started
Just using rust no cargo

each day has README.md with instructions and test_input.txt and also puzzle_input.txt

usage

```
rustc <day>/rust/<day>.rs
./<day> <day>/puzzle_input.txt
```

