use std::env;
use std::fs;

fn find_gamma_bit(input: &[char]) -> char {
    let mut occurences: [u32; 2] = [0, 0];
    for c in input {
        if *c == '0' {
            occurences[0] += 1;
        } else {
            occurences[1] += 1;
        }
    }
    if occurences[0] > occurences[1] {
        return '0'
    } else {
        return '1'
    }
}

fn solve_part1(values: &[Vec<char>]) -> u32 {
    let mut bits: Vec<Vec<char>> = vec![Vec::new();values[0].len()];
    for value in values {
        for (i, bit) in value.iter().enumerate() {
            bits[i].push(*bit)
        }
    }

    // Find most common bit in every column
    let gamma_raw: Vec<char> = bits.iter().map(|column| find_gamma_bit(column)).collect();

    let gamma = u32::from_str_radix(
            &gamma_raw.iter()
            .map(std::string::ToString::to_string)
            .collect::<String>(),
            2,
    ).expect("Error parsing gama raw");

    // Epsilon is inverse of gamma
    let epsilon_raw: Vec<char> = gamma_raw.iter()
        .map(|c| if *c == '0' { '1' } else { '0' })
        .collect();

    let epsilon = u32::from_str_radix(
            &epsilon_raw.iter()
            .map(std::string::ToString::to_string)
            .collect::<String>(),
            2,
    ).expect("Error parsing epsilon raw");

    return gamma * epsilon;
}

fn get_col<T: Copy>(v: &[Vec<T>], wanted_col_idx: usize) -> Vec<T> {
    // Return only the one column of `v` that is wanted_col_idx
    let mut result = Vec::new();
    for row in v.iter() {
        for (col_idx, &item) in row.iter().enumerate() {
            if col_idx == wanted_col_idx {
                result.push(item);
            }
        }
    }
    return result;
}

fn bit_criteria(values: &Vec<Vec<char>>, o2: bool) -> u32 {
    let mut values_to_keep = values.to_owned();
    let nr_of_bits: usize = values[0].len();

    for bit in 0..nr_of_bits {
        let column = get_col(&values_to_keep, bit);
        let gamma_bit = find_gamma_bit(&column);
        let mut epsilon_bit: char = '0';
        if gamma_bit == '0' {
            epsilon_bit = '1';
        }

        if o2 {
            values_to_keep.retain(|values_to_keep| values_to_keep[bit] == gamma_bit);
        } else {
            values_to_keep.retain(|values_to_keep| values_to_keep[bit] == epsilon_bit);
        }

        if values_to_keep.len() == 1 {
            break;
        }
    }

    let result = u32::from_str_radix(
            &values_to_keep[0]
            .iter()
            .map(std::string::ToString::to_string)
            .collect::<String>(),
            2,
        ).expect("Error parsing gama raw");
    return result;
}

fn solve_part2(values: &Vec<Vec<char>>) -> u32 {
    let o2_rating = bit_criteria(values, true);
    let co2_rating = bit_criteria(values, false);

    return o2_rating * co2_rating;

}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading file");

    let values: Vec<Vec<char>> = contents.lines()
        .map(|line| line.chars().collect())
        .collect();
   
    //Part 1
    let res = solve_part1(&values);
    println!("Power consumption is {}", res);

    //Part 2
    let res2 = solve_part2(&values);
    println!("Life support Rating is: {}", res2)
}
