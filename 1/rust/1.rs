use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let mut val: u32 = 0;

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let values = contents.lines().map(|line| line.parse::<u32>().unwrap()).collect::<Vec<u32>>();

    // Part1
    for i in 0..=values.len() {
        if i > 0 && values.get(i) > values.get(i-1) {
            val += 1
        }
    }
    println!("Part1: Measurements larger than previous: {}", val);
    // Part2
    val = 0;
    for i in (3..=values.len()).step_by(1) {
        if i > 0 && values.get(i) > values.get(i-3) {
            val += 1;
        }
    }
    println!("Part2: Measurements larger than previous: {}", val);
}
