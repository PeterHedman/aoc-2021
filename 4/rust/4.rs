use std::env;
use std::fs;

type Board = Vec<Vec<i32>>;

fn has_bingo(board: &Board) -> bool {
    let row = board.iter().any(|r| r.iter().all(|n| *n == -1));
    let col = (0..5).any(|i| board.iter().all(|c| c[i] == -1));

    row || col
}

fn play(boards_in: Vec<Board>, values: Vec<i32>, win: bool) -> i32 {
    let mut boards: Vec<Board> = boards_in.clone();
    let mut result = 0;
    for i in 0..values.len() {
        for board in &mut boards {
            for x in 0..board.len() {
                for y in 0..board[0].len() {
                    if board[x][y] == values[i] {
                            board[x][y] = -1;
                    }
                }
            }
            if has_bingo(&board) && !board.is_empty() {
                let sum: i32 = board
                    .iter()
                    .map(|r| r.iter().filter(|i| **i >= 0).sum::<i32>())
                    .sum();
                    //println!("Part1: Val is: {} {}", values[i], sum);
                    //println!("Part1: Val is: {}", values[i]*sum);
                result = values[i] * sum;
                if win {
                    return result
                }
                board.clear();
            }
        }
    }
    return result;
}

fn main() {
    // PArse Input
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let part = &args[2];

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
    //Bing srwan numbers
    let values: Vec<i32> = contents.lines()
                                    .next()
                                    .unwrap()
                                    .split(',')
                                    .map(|string| string.parse::<i32>().unwrap())
                                    .collect();

    let mut boards: Vec<Board> = Vec::new();

    //Bingo Boards
    for board_nr in contents.split("\n\n").skip(1) {
        let board_val = board_nr
            .lines()
            .map(|l| {
                l.split_whitespace()
                    .map(|c| c.parse::<i32>().unwrap())
                    .collect::<Vec<i32>>()
            })
            .collect::<Vec<Vec<i32>>>();

        boards.push(board_val);
    }

    //Part 1
    if part == "1" {
        let res = play(boards, values, true);
        println!("Part1: Val is: {}", res);
    } else {
        // Part2
        let res2 = play(boards, values, false);
        println!("Part2: Val is: {}", res2);
    }
}
