use std::env;
use std::fs;

const RADIX: u32 = 10;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let mut x = 0;
    let mut y = 0;

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let values: Vec<&str> = contents.lines().collect();

    //Part 1
    for i in 0..values.len() {
        let val = values[i].chars().last().unwrap().to_digit(RADIX).unwrap();
        if values[i].contains("forward") {
            x += val;
        } else if values[i].contains("up") && y > val {
            y -= val;
        } else if values[i].contains("down")  {
            y += val;
        }
    }

    let res = x * y;
    println!("Part1: Val is: {}", res);

    // Part2
    let mut aim = 0;
    y = 0;
    x = 0;

    for i in 0..values.len() {
        let val = values[i].chars().last().unwrap().to_digit(VAL).unwrap();
        if values[i].contains("forward") {
            x += val;
            y += aim * val;
        } else if values[i].contains("up") && y > val {
            aim -= val;
        } else if values[i].contains("down")  {
            aim += val;
        }
    }
    
    let res2 = x * y;
    println!("Part2: Val is: {}", res2);
}
