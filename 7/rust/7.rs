use std::env;
use std::fs;

fn solve_part_1(values: Vec<i64>) -> i64 {
    let mut least_cost: i64 = 9999999;
    for i in 0..values.len() {
        let mut cost = 0;
        for j in 0..values.len() {
            let diff = values[i] - values[j];
            cost += diff.abs();
        }
        if cost <= least_cost {
            least_cost = cost;
        }
    }
    return least_cost;
}

fn solve_part_2(values: Vec<i64>) -> i64 {
    let mut least_cost: i64 = 9999999999;
    let mut cost: i64 = 0;
    let sum: i64 = values.iter().sum();
    let medium: i64 = sum/values.len() as i64;
    for j in 0..values.len() {
        cost += (1..=(values[j]-medium).abs()).fold(0, |a, b| a + b);
    }
    if cost <= least_cost {
        least_cost = cost;
    }
    return least_cost;
}

fn main() {
    // PArse Input
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let part = &args[2];

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
    //Bing srwan numbers
    let values = contents.split(',').map(|s| s.trim().parse::<i64>().unwrap()).collect::<Vec<i64>>();

    println!("{:?}", values);

    if part == "1" {
        // Part1
        let res = solve_part_1(values);
        println!("Part1: Val is: {}", res);
    } else {
        // Part2
        let res2 = solve_part_2(values);
        println!("Part2: Val is: {}", res2);
    }
}
